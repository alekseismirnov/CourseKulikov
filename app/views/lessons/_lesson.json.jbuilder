json.extract! lesson, :id, :title, :time, :teacher, :room, :day_week, :course, :group, :created_at, :updated_at
json.url lesson_url(lesson, format: :json)
