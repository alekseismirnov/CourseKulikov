class Lesson < ApplicationRecord
  validates :title, :time, :teacher, :day_week, :room, :course, :group, presence: true
  scope :lesson_select, -> { select(:id, :title, :time, :teacher, :day_week, :room, :course, :group, :numerator) }
end
