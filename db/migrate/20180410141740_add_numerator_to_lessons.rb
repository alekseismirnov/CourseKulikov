class AddNumeratorToLessons < ActiveRecord::Migration[5.1]
  def change
    add_column :lessons, :numerator, :string
  end
end
