class CreateLessons < ActiveRecord::Migration[5.1]
  def change
    create_table :lessons do |t|
      t.string :title
      t.string :time
      t.string :teacher
      t.string :room
      t.string :day_week
      t.integer :course
      t.integer :group

      t.timestamps
    end
  end
end
